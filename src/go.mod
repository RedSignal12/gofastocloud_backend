module gofastocloud_backend

go 1.16

require (
	github.com/barasher/go-exiftool v1.7.0
	github.com/dgrijalva/jwt-go/v4 v4.0.0-preview1
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.5.0
	github.com/recoilme/pudge v1.0.3
	github.com/rs/cors v1.8.2
	github.com/shirou/gopsutil/v3 v3.22.5
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.8.0
	gitlab.com/fastogt/gofastocloud v1.8.7
	gitlab.com/fastogt/gofastocloud_base v1.6.2
	gitlab.com/fastogt/gofastocloud_http v1.3.2
	gitlab.com/fastogt/gofastocloud_models v0.5.15
	gitlab.com/fastogt/gofastogt v1.3.0
	go.mongodb.org/mongo-driver v1.9.1
	gopkg.in/yaml.v3 v3.0.1
	gortc.io/sdp v0.18.2
)
