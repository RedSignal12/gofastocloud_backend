package unit

import (
	"gofastocloud_backend/app/auth"
	"testing"

	"github.com/stretchr/testify/assert"
)

//JWT Token
func TestJWTContentAuthManager_CheckAuthContent(t *testing.T) {
	type args struct {
		token string
		ip    string
	}
	//setting validToken
	authContentManager := &auth.JWTContentAuthManager{}
	data := "127.0.0.1"
	validToken, _ := authContentManager.GetAuthContentToken(data)
	invalidToken := *validToken + "0"
	expiredToken := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NTUzMDkwODMsImlwIjoiMTI3LjAuMC4xIn0.HwcravKNtBNdeEKrdo1Y8QoXBdfue_YaXawpecfnzX9"

	tests := []struct {
		name      string
		c         *auth.JWTContentAuthManager
		args      args
		assertion assert.ErrorAssertionFunc
	}{
		{name: "valid",
			c:         authContentManager,
			args:      args{token: *validToken, ip: data},
			assertion: assert.NoError,
		},
		{name: "invalid",
			c:         authContentManager,
			args:      args{token: invalidToken, ip: data},
			assertion: assert.Error,
		},
		{name: "expired",
			c:         authContentManager,
			args:      args{token: expiredToken, ip: data},
			assertion: assert.Error,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := tt.c.CheckAuthContent(tt.args.token, tt.args.ip)
			tt.assertion(t, err)
		})
	}

}
func TestJWTContentAuthManager_GetAuthContentToken(t *testing.T) {
	type args struct {
		data string
	}
	tests := []struct {
		name string
		c    *auth.JWTContentAuthManager
		args args
	}{
		{name: "valid",
			c:    &auth.JWTContentAuthManager{},
			args: args{data: "127.0.0.1"},
		},
	}
	for _, tt := range tests {
		_, err := tt.c.GetAuthContentToken(tt.args.data)
		assert.NoError(t, err)
	}
}

//Base64 Token
func TestBaseContentAuthMenager_CheckAuthContent(t *testing.T) {
	//setting token

	authContentManager := &auth.BaseContentAuthMenager{}
	data := "127.0.0.1"
	validToken, _ := authContentManager.GetAuthContentToken(data)
	expiredToken := "MTI3LjAuMC4xOjE2NTUzNjMyOTQ0NjM="

	type args struct {
		token string
		ip    string
	}
	tests := []struct {
		name      string
		b         *auth.BaseContentAuthMenager
		args      args
		assertion assert.ErrorAssertionFunc
	}{
		{name: "valid",
			b: authContentManager,
			args: args{token: *validToken,
				ip: data},
			assertion: assert.NoError},
		{name: "expired",
			b: authContentManager,
			args: args{token: expiredToken,
				ip: data},
			assertion: assert.Error},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := tt.b.CheckAuthContent(tt.args.token, tt.args.ip)
			tt.assertion(t, err)
		})
	}
}

func TestBaseContentAuthMenager_GetAuthContentToken(t *testing.T) {
	type args struct {
		data string
	}
	tests := []struct {
		name string
		b    *auth.BaseContentAuthMenager
		args args
	}{
		{name: "valid",
			b:    &auth.BaseContentAuthMenager{},
			args: args{data: "127.0.0.1"},
		},
	}
	for _, tt := range tests {
		_, err := tt.b.GetAuthContentToken(tt.args.data)
		assert.NoError(t, err)
	}
}
