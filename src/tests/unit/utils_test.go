package unit

import (
	"gofastocloud_backend/app/utils"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSplitHlsUrl(t *testing.T) {
	type args struct {
		url string
	}
	tests := []struct {
		name           string
		args           args
		wantTypeStream int
		wantSid        string
		wantOid        int
		wantName       string
		assertionErr   assert.ErrorAssertionFunc
	}{
		{name: "Valid",
			args:           args{url: "2/625ce9511860b729203f2695/0/master.m3u8"},
			wantTypeStream: 2,
			wantSid:        "625ce9511860b729203f2695",
			wantOid:        0,
			wantName:       "master.m3u8",
			assertionErr:   assert.NoError,
		},
		{name: "invalid",
			args:           args{url: "http:/2/625ce9511860b729203f2695/0/master.m3u8"},
			wantTypeStream: 0,
			wantSid:        "",
			wantOid:        0,
			wantName:       "",
			assertionErr:   assert.Error,
		},
		{name: "invalid",
			args:           args{url: "https://tile.loc.gov/streaming-services/iiif/service:mbrs:ntscrm:00068306:00068306/full/full/0/full/default.m3u8"},
			wantTypeStream: 0,
			wantSid:        "",
			wantOid:        0,
			wantName:       "",
			assertionErr:   assert.Error,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			gotTypeStream, gotSid, gotOid, gotFileName, err := utils.SplitHlsUrl(tt.args.url)
			tt.assertionErr(t, err)
			assert.Equal(t, gotTypeStream, tt.wantTypeStream)
			assert.Equal(t, gotFileName, tt.wantName)
			assert.Equal(t, gotSid, tt.wantSid)
			assert.Equal(t, gotOid, tt.wantOid)
		})
	}
}

func TestIsValidFileName(t *testing.T) {
	type args struct {
		fileName string
	}
	tests := []struct {
		name      string
		args      args
		assertion assert.BoolAssertionFunc
	}{
		{name: "True_1",
			args:      args{"master.m3u8"},
			assertion: assert.True},
		{name: "True_2",
			args:      args{"master.ts"},
			assertion: assert.True},
		{name: "False_1",
			args:      args{"master.inf"},
			assertion: assert.False},
		{name: "False_2",
			args:      args{"master"},
			assertion: assert.False},
		{name: "False_3",
			args:      args{"master.mmm.bbb"},
			assertion: assert.False},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.assertion(t, utils.IsHlsFileName(tt.args.fileName))
		})
	}
}
