package app

import (
	"net/http"

	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
)

func (app *App) ServeWebRTCWebRTCIn(w http.ResponseWriter, r *http.Request) {
	u := websocket.Upgrader{CheckOrigin: func(r *http.Request) bool { return true }}
	c, err := u.Upgrade(w, r, nil)
	if err != nil {
		log.Errorf("ws webrtc in error: %s", err.Error())
		return
	}

	ws := app.wsInWebRTCManager.CreateWsWebRTCClientConnection(c)

	log.Debugf("New client ws in")
	ws.WriteMessage("You are welcome to FastoCloud WebRTC in websocket!")
	defer ws.Close()

	for {
		_, message, err := c.ReadMessage()

		if err != nil {
			log.Errorf("ws webrtc in read error: %s", err.Error())
			break
		}

		err = app.wsInWebRTCManager.MessageWebRTCProcess(message, ws)
		if err != nil {
			log.Errorf("ws webrtc in process error: %s", err.Error())
			break
		}
	}
}
