package storage

import (
	"encoding/json"

	"github.com/recoilme/pudge"
)

type Pudge struct {
	IStorage

	handle *pudge.Db
}

func NewPudge(path string) (*Pudge, error) {
	cfg := &pudge.Config{SyncInterval: 0} // disable every second fsync
	db, err := pudge.Open(path, cfg)
	if err != nil {
		return nil, err
	}

	pudge := Pudge{handle: db}
	return &pudge, nil
}

func (storage *Pudge) Set(key string, value json.RawMessage) error {
	return storage.handle.Set(key, value)
}

func (storage *Pudge) Get(key string) (json.RawMessage, error) {
	var stream json.RawMessage
	err := storage.handle.Get(key, &stream)
	if err != nil {
		return nil, err
	}

	return stream, nil
}

func (storage *Pudge) Delete(key string) error {
	err := storage.handle.Delete(key)
	if err != nil {
		return err
	}
	return nil
}

func (storage *Pudge) GetStreams() ([]json.RawMessage, error) {
	k, err := storage.handle.Keys(nil, 0, 0, false)
	if err != nil {
		return nil, err
	}
	var streams []json.RawMessage
	for _, key := range k {
		var stream json.RawMessage
		err := storage.handle.Get(key, &stream)
		if err == nil {
			streams = append(streams, stream)
		}
	}
	for i, j := 0, len(streams)-1; i < j; i, j = i+1, j-1 {
		streams[i], streams[j] = streams[j], streams[i]
	}
	return streams, nil
}

func (storage *Pudge) Close() error {
	return storage.handle.Close()
}
