package storage

import (
	"encoding/json"
)

type IStorage interface {
	Set(key string, value json.RawMessage) error
	Get(key string) (json.RawMessage, error)
	Delete(key string) error
	Close() error
	GetStreams() ([]json.RawMessage, error)
}
