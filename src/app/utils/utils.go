package utils

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/barasher/go-exiftool"
	"github.com/rs/cors"
	"gitlab.com/fastogt/gofastocloud/media"
	"gitlab.com/fastogt/gofastogt"
)

// "0:02:15" or "1.51 s"
func timeParse(st string) (*gofastogt.DurationMsec, error) {
	var duration float64
	n, err := fmt.Sscanf(st, "%f s", &duration)
	if err != nil || n != 1 {
		var h, m, s int
		n, err := fmt.Sscanf(st, "%d:%d:%d", &h, &m, &s)
		if err != nil || n != 3 {
			return nil, err
		}
		stabled := gofastogt.DurationMsec(h*3600+m*60+s) * 1000
		return &stabled, nil
	}
	stabled := gofastogt.DurationMsec(duration * 1000)
	return &stabled, nil
}

func NewHttpServer(addr string, corsEnabled bool, handler http.Handler) *http.Server {
	if corsEnabled {
		options := cors.Options{
			AllowedOrigins:   []string{"*"},
			AllowedHeaders:   []string{"*"},
			AllowCredentials: true,
		}
		c := cors.New(options)
		corsHandler := c.Handler(handler)
		return &http.Server{Addr: addr, Handler: corsHandler}
	}
	return &http.Server{Addr: addr, Handler: handler}
}

func GetMediaFileInfo(path string) (*media.MediaUrlInfo, error) {
	file, err := gofastogt.MakeFilePathProtocol(path)
	if err != nil {
		return nil, err
	}

	et, err := exiftool.NewExiftool()
	if err != nil {
		return nil, err
	}

	defer et.Close()

	fileInfos := et.ExtractMetadata(file.GetPath())
	if len(fileInfos) == 0 {
		return nil, errors.New("invalid meta")
	}

	fileInfo := fileInfos[0]
	if fileInfo.Err != nil {
		return nil, fileInfo.Err
	}

	var rotation *int
	rotationVal, err := fileInfo.GetInt("Rotation")
	if err != nil {
		if err != exiftool.ErrKeyNotFound {
			return nil, fmt.Errorf("failed to get rotation (%v)", err)
		}
		intr := 0
		rotation = &intr
	} else {
		intr := int(rotationVal)
		rotation = &intr
	}

	imageWidth, err := fileInfo.GetInt("ImageWidth")
	if err != nil {
		return nil, fmt.Errorf("failed to get width (%v)", err)
	}

	imageHeight, err := fileInfo.GetInt("ImageHeight")
	if err != nil {
		return nil, fmt.Errorf("failed to get height (%v)", err)
	}

	mediaDuration, err := fileInfo.GetString("MediaDuration")
	if err != nil {
		if err != exiftool.ErrKeyNotFound {
			return nil, fmt.Errorf("failed to get duration (%v)", err)
		}
		mediaDuration, err = fileInfo.GetString("Duration")
		if err != nil {
			return nil, fmt.Errorf("failed to get duration (%v)", err)
		}
	}

	duration, err := timeParse(mediaDuration)
	if err != nil {
		return nil, fmt.Errorf("failed to get time (%v)", err)
	}

	media := media.NewMediaUrlInfo(media.MediaInfo{Rotation: rotation, Width: int(imageWidth),
		Height: int(imageHeight), Duration: *duration}, file.GetFilePath())
	return media, nil
}

func SplitHlsUrl(path string) (typeStream int, sid string, oid int, hlsFile string, err error) {
	split := strings.Split(path, "/")
	if len(split) != 4 {
		return 0, "", 0, "", errors.New("invalid Url")
	}
	typeStream, err = strconv.Atoi(split[0])
	if err != nil {
		return 0, "", 0, "", errors.New("invalid Url")
	}
	oid, err = strconv.Atoi(split[2])
	if err != nil {
		return 0, "", 0, "", errors.New("invalid Url")
	}
	return typeStream, split[1], oid, split[3], nil
}

func IsHlsFileName(fileName string) bool {
	name := strings.Split(fileName, ".")
	if len(name) != 2 {
		return false
	}
	return name[1] == "ts" || name[1] == "m3u8"
}
