package app

import (
	"encoding/json"
	"fmt"
	"gofastocloud_backend/app/errorgt"
	"gofastocloud_backend/app/utils"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"

	"gitlab.com/fastogt/gofastocloud_http/fastocloud/public"
	"gitlab.com/fastogt/gofastogt"
)

func (app *App) generateUniqueUploadFilePath(file_name string) (*string, error) {
	if app.uploads_folder == nil {
		return nil, fmt.Errorf("uploads folder not inited")
	}

	extension := filepath.Ext(file_name)
	ts := gofastogt.MakeUTCTimestamp()
	generated_file_name := fmt.Sprintf("%d%s", ts, extension)
	path := filepath.Join(*app.uploads_folder, generated_file_name)
	return &path, nil
}

func (app *App) getStreamPipelineFilePath(sid string) (*string, error) {
	return app.generateStreamFilePathWithName(sid + "_pipeline.html")
}

func (app *App) getStreamConfigFilePath(sid string) (*string, error) {
	return app.generateStreamFilePathWithName(sid + "_config.json")
}

func (app *App) getStreamLogsFilePath(sid string) (*string, error) {
	return app.generateStreamFilePathWithName(sid + "_logs")
}

func (app *App) generateStreamFilePathWithName(sid string) (*string, error) {
	if app.streams_fodler == nil {
		return nil, fmt.Errorf("streams folder not inited")
	}
	path := filepath.Join(*app.streams_fodler, sid)
	return &path, nil
}

func (app *App) generateRuntimeFilePathWithName(fileName string) (*string, error) {
	if app.runtime_fodler == nil {
		return nil, fmt.Errorf("runtime folder not inited")
	}
	path := filepath.Join(*app.runtime_fodler, fileName)
	return &path, nil
}

func (app *App) getServerLogsFilePath() (*string, error) {
	return app.generateRuntimeFilePathWithName("logs")
}

func (app *App) generateUniqueUploadFilePathWithName(fileName string) (*string, error) {
	if app.uploads_folder == nil {
		return nil, fmt.Errorf("uploads folder not inited")
	}
	path := filepath.Join(*app.uploads_folder, fileName)
	return &path, nil
}

func (app *App) canBeRemovedUploadedFile(path string) bool {
	if app.uploads_folder == nil {
		return false
	}

	dir := filepath.Dir(path)
	return *app.uploads_folder == dir
}

func (app *App) UploadVideoFile(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	maxBytes := app.config.Settings.MaxUploadFileSize * 1024 * 1024
	r.Body = http.MaxBytesReader(w, r.Body, maxBytes)
	if err := r.ParseMultipartForm(maxBytes); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	// The argument to FormFile must match the name attribute
	// of the file input on the frontend
	file, fileHeader, err := r.FormFile("file")
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}
	defer file.Close()

	var filePath *string
	fileName := r.FormValue("meta")

	if len(fileName) != 0 {
		filePath, err = app.generateUniqueUploadFilePathWithName(fileName)
	} else {
		filePath, err = app.generateUniqueUploadFilePath(fileHeader.Filename)
	}

	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// Create a new file in the uploads directory
	dst, err := os.Create(*filePath)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// Copy the uploaded file to the filesystem
	// at the specified destination
	_, err = io.Copy(dst, file)
	dst.Close()
	if err != nil {
		os.Remove(*filePath)
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	fileInfo, err := utils.GetMediaFileInfo(*filePath)
	if err != nil {
		os.Remove(*filePath)
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	resp := public.UploadFile{Info: *fileInfo}
	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(resp))
}

func (app *App) RemoveVideoFile(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	var result public.RemoveVideoRequest
	if err := json.Unmarshal(body, &result); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	if !app.canBeRemovedUploadedFile(result.Path) {
		respondWithError(w, http.StatusForbidden, errorgt.MakeErrorJsonForrbiddenAction(nil))
		return
	}

	err = os.Remove(result.Path)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	respondWithOk(w)
}
