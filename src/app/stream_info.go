package app

import (
	"gofastocloud_backend/app/common"

	"gitlab.com/fastogt/gofastocloud/media"
)

type StreamInfo struct {
	Statistic *media.StreamStatisticInfo
	Config    *common.StreamConfig
}

func (info *StreamInfo) IsActive() bool {
	return info.Statistic != nil
}

func (info *StreamInfo) HasConfig() bool {
	return info.Config != nil
}

func NewStreamInfo(config *common.StreamConfig, statistic *media.StreamStatisticInfo) *StreamInfo {
	info := StreamInfo{Statistic: statistic, Config: config}
	return &info
}
