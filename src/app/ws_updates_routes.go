package app

import (
	"net/http"

	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
)

func (app *App) ServeUpdatesWs(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequestWithToken(r)
	if err != nil {
		http.Error(w, "Not Authorized", http.StatusUnauthorized)
		return
	}

	u := websocket.Upgrader{CheckOrigin: func(r *http.Request) bool { return true }}
	c, err := u.Upgrade(w, r, nil)
	if err != nil {
		log.Errorf("ws update error: %s", err.Error())
		return
	}

	ws := app.wsUpdatesManager.CreateWsUpdatesCClientConnection(c)
	app.wsUpdatesManager.RegisterWsUpdatesClientConnection(ws)
	log.Debugf("New client ws update")
	ws.WriteMessage("You are welcome to FastoCloud updates WebSocket!")
	defer ws.Close()

	for {
		_, message, err := c.ReadMessage()

		if err != nil {
			log.Errorf("ws update read error: %s", err.Error())
			break
		}

		err = app.wsUpdatesManager.MessageUpdatesProcess(message, ws)
		if err != nil {
			log.Errorf("ws update process error: %s", err.Error())
			break
		}
	}
}
