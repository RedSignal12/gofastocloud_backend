package app

import (
	"fmt"
	"gofastocloud_backend/app/webrtc_in"
	"gofastocloud_backend/app/webrtc_out"
	"io"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/fastogt/gofastocloud/media"
	"gitlab.com/fastogt/gofastocloud_base"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models"
	"gitlab.com/fastogt/gofastogt"
)

type IRunner interface {
	Run()
	Stop()
}

type WrapNode struct {
	IRunner
	webrtc_out.IWebRTCOut
	webrtc_in.IWebRTCIn

	quit chan bool

	node    *gofastocloud_models.NodeSettings
	handler media.IFastoCloudNodeClientHandler

	// runtime
	client *media.FastoCloudNodeClient // fastocloud client
}

func NewWrapNode(node *gofastocloud_models.NodeSettings, handler media.IFastoCloudNodeClientHandler) *WrapNode {
	wrap := WrapNode{node: node, handler: handler, quit: make(chan bool)}
	return &wrap
}

func (wrap *WrapNode) Run() {
	log.Info("Started wrap node loop")
	if wrap.node == nil {
		<-wrap.quit
		log.Info("Finished wrap node loop")
		return
	}
	attempt := 0
	for {
		log.Infof("fastocloud have started connection loop delay: %v", attempt)
		select {
		case <-wrap.quit:
			log.Info("Finished wrap node loop")
			return
		case <-time.After(time.Duration(attempt) * time.Second):
			log.Info("trying to connect to fastocloud service")
			err := wrap.connectClient()
			if err == nil {
				for {
					data, err := wrap.client.ReadCommand()
					if err != nil {
						if err != io.EOF {
							log.Errorf("fastocloud read error: %s", err.Error())
						}
						// reconnect
						break
					}
					wrap.client.ProcessCommand(data)
				}
				wrap.resetConnectClient()
			} else {
				log.Error(err)
			}
			attempt = 60
		}
	}
}

func (wrap *WrapNode) resetConnectClient() {
	if wrap.client != nil {
		wrap.client.Flush()
		wrap.client = nil
	}
}

func (wrap *WrapNode) disConnectClient() {
	if wrap.client != nil {
		wrap.client.Disconnect()
		wrap.client = nil
	}
}

func (wrap *WrapNode) connectClient() error {
	host := wrap.node.GetHost()
	if host == nil {
		return gofastogt.ErrInvalidInput
	}

	client := media.NewFastoCloudNodeClient(*host, wrap.handler)
	err := client.Connect()
	if err != nil {
		return fmt.Errorf("fastocloud connect error: %s", err.Error())
	}

	_, err = client.Activate(media.NewActivateRequest(wrap.node.Key))
	if err != nil {
		client.Disconnect()
		return fmt.Errorf("fastocloud activate error: %s", err.Error())
	}
	wrap.client = client
	return nil
}

func (wrap *WrapNode) Stop() {
	wrap.disConnectClient()
	wrap.quit <- true
}

func (wrap *WrapNode) IsActive() bool {
	if wrap.client == nil {
		return false
	}
	return wrap.client.IsActive()
}

func (wrap *WrapNode) ProbeOutStreamWithCallback(request *media.ProbeOutStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.ProbeOutStreamWithCallback(request, callback)
}

func (wrap *WrapNode) ScanFolderWithCallback(request *media.ScanFolderRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.ScanFolderWithCallback(request, callback)
}

func (wrap *WrapNode) RestartStreamWithCallback(request *media.RestartStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.RestartStreamWithCallback(request, callback)
}

func (wrap *WrapNode) StopStreamWithCallback(request *media.StopStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.StopStreamWithCallback(request, callback)
}

func (wrap *WrapNode) GetLogServiceWithCallback(request *media.GetLogServiceRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.GetLogServiceWithCallback(request, callback)
}

func (wrap *WrapNode) StartStreamWithCallback(request *media.StartStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.StartStreamWithCallback(request, callback)
}

func (wrap *WrapNode) StartStream(request *media.StartStreamRequest) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.StartStream(request)
}

func (wrap *WrapNode) RemoveMasterInputUrlWithCallback(request *media.RemoveMasterInputUrlRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.RemoveMasterInputUrlWithCallback(request, callback)
}

func (wrap *WrapNode) InjectMasterInputUrlWithCallback(request *media.InjectMasterInputUrlRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.InjectMasterInputUrlWithCallback(request, callback)
}

func (wrap *WrapNode) GetPipelineStreamWithCallback(request *media.GetPipelineStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.GetPipelineStreamWithCallback(request, callback)
}

func (wrap *WrapNode) GetLogStreamWithCallback(request *media.GetLogStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.GetLogStreamWithCallback(request, callback)
}

func (wrap *WrapNode) GetConfigJsonStreamWithCallback(request *media.GetConfigJsonStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.GetConfigJsonStreamWithCallback(request, callback)
}

func (wrap *WrapNode) ChangeInputStreamWithCallback(request *media.ChangeInputStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.ChangeInputStreamWithCallback(request, callback)
}

func (wrap *WrapNode) CleanStreamWithCallback(request *media.CleanStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.CleanStreamWithCallback(request, callback)
}

func (wrap *WrapNode) ProbeInStreamWithCallback(request *media.ProbeInStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.ProbeInStreamWithCallback(request, callback)
}

func (wrap *WrapNode) MountS3BucketWithCallback(request *media.MountS3BucketRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.MountS3BucketWithCallback(request, callback)
}

func (wrap *WrapNode) UnMountS3BucketWithCallback(request *media.UnMountS3BucketRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.UnMountS3BucketWithCallback(request, callback)
}

func (wrap *WrapNode) ScanS3BucketsWithCallback(request *media.ScanS3BucketsRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.ScanS3BucketsWithCallback(request, callback)
}

// webrtc

func (wrap *WrapNode) WebRTCOutInitStream(web *media.WebRTCOutInitRequest) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.WebRTCOutInitStream(web)
}

func (wrap *WrapNode) WebRTCOutSDPStream(web *media.WebRTCOutSdpRequest) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.WebRTCOutSDPStream(web)
}

func (wrap *WrapNode) WebRTCOutICEStream(web *media.WebRTCOutIceRequest) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.WebRTCOutICEStream(web)
}

func (wrap *WrapNode) WebRTCOutDeinitStream(web *media.WebRTCOutDeInitRequest) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.WebRTCOutDeinitStream(web)
}

func (wrap *WrapNode) WebRTCInDeinitStream(web *media.WebRTCInDeInitRequest) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.WebRTCInDeinitStream(web)
}

func (wrap *WrapNode) WebRTCInInitStream(web *media.WebRTCInInitRequest) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.WebRTCInInitStream(web)
}

func (wrap *WrapNode) WebRTCInSDPStream(web *media.WebRTCInSdpRequest) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.WebRTCInSDPStream(web)
}

func (wrap *WrapNode) WebRTCInICEStream(web *media.WebRTCInIceRequest) (*gofastocloud_base.RPCId, error) {
	if wrap.client == nil {
		return nil, gofastogt.ErrInvalidInput
	}
	return wrap.client.WebRTCInICEStream(web)
}

func (wrap *WrapNode) GetVodsHost() *gofastogt.HostAndPort {
	if wrap.client == nil {
		return nil
	}

	return wrap.client.GetVodsHost()
}

func (wrap *WrapNode) GetCodsHost() *gofastogt.HostAndPort {
	if wrap.client == nil {
		return nil
	}

	return wrap.client.GetCodsHost()
}

func (wrap *WrapNode) GetHttpHost() *gofastogt.HostAndPort {
	if wrap.client == nil {
		return nil
	}

	return wrap.client.GetHttpHost()
}

func (wrap *WrapNode) GetRuntimeStats() *media.ServiceStatisticInfo {
	if wrap.client == nil {
		return nil
	}

	return wrap.client.GetRuntimeStats()
}

func (wrap *WrapNode) GetProject() *string {
	if wrap.client == nil {
		return nil
	}

	return wrap.client.GetProject()
}

func (wrap *WrapNode) GetVersion() *string {
	if wrap.client == nil {
		return nil
	}

	return wrap.client.GetVersion()
}

func (wrap *WrapNode) GetExpirationTime() *gofastogt.UtcTimeMsec {
	if wrap.client == nil {
		return nil
	}

	return wrap.client.GetExpirationTime()
}

func (wrap *WrapNode) GetOS() *gofastocloud_base.OperationSystem {
	if wrap.client == nil {
		return nil
	}

	return wrap.client.GetOS()
}

func (wrap *WrapNode) GetStatus() *gofastocloud_base.ConnectionStatus {
	if wrap.client == nil {
		return nil
	}

	stat := wrap.client.GetStatus()
	return &stat
}
