package app

import (
	"io/ioutil"
	"os"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models"
	"gopkg.in/yaml.v3"
)

type Config struct {
	Settings gofastocloud_models.GoBackendSettings
}

func SaveConfig(s *Config, configPath string) error {
	conf, err := os.Create(configPath)
	if err != nil {
		return err
	}
	defer conf.Close()

	err = yaml.NewEncoder(conf).Encode(s)
	if err != nil {
		return err
	}

	return nil
}

func LoadConfig(configPath string) (*Config, error) {
	buf, err := ioutil.ReadFile(configPath)
	if err != nil {
		return nil, err
	}

	var s Config
	err = yaml.Unmarshal(buf, &s)
	if err != nil {
		return nil, err
	}
	return &s, nil
}
