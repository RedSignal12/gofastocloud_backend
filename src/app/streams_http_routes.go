package app

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"text/template"

	log "github.com/sirupsen/logrus"

	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"gofastocloud_backend/app/common"
	"gofastocloud_backend/app/errorgt"
	"gofastocloud_backend/app/utils"

	"github.com/gorilla/mux"
	"gitlab.com/fastogt/gofastocloud/media"
	"gitlab.com/fastogt/gofastocloud_base"
	"gitlab.com/fastogt/gofastocloud_http/fastocloud/public"
	"gitlab.com/fastogt/gofastocloud_models/constans"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

const kDefaultCodHlsChunkDuration = 5

// need update if new type have been added
func getConfigFromDBModels(data json.RawMessage) ([]byte, error) {
	var istream gofastocloud_models.IStream
	if err := json.Unmarshal(data, &istream); err != nil {
		return nil, err
	}

	if istream.TypeStream == constans.PROXY_STR {
		var proxyStream gofastocloud_models.ProxyStream
		if err := json.Unmarshal(data, &proxyStream); err != nil {
			return nil, err
		}

		return json.Marshal(proxyStream.GetConfig())
	} else if istream.TypeStream == constans.VOD_PROXY_STR {
		var proxyStream gofastocloud_models.VodProxyStream
		if err := json.Unmarshal(data, &proxyStream); err != nil {
			return nil, err
		}

		return json.Marshal(proxyStream.GetConfig())
	} else if istream.TypeStream == constans.RELAY_STR {
		var relayStream gofastocloud_models.RelayStream
		if err := json.Unmarshal(data, &relayStream); err != nil {
			return nil, err
		}

		return json.Marshal(relayStream.GetConfig())
	} else if istream.TypeStream == constans.VOD_RELAY_STR {
		var relayStream gofastocloud_models.VodRelayStream
		if err := json.Unmarshal(data, &relayStream); err != nil {
			return nil, err
		}

		return json.Marshal(relayStream.GetConfig())
	} else if istream.TypeStream == constans.ENCODE_STR {
		var encoderStream gofastocloud_models.EncodeStream
		if err := json.Unmarshal(data, &encoderStream); err != nil {
			return nil, err
		}

		return json.Marshal(encoderStream.GetConfig())
	} else if istream.TypeStream == constans.VOD_ENCODE_STR {
		var encoderStream gofastocloud_models.VodEncodeStream
		if err := json.Unmarshal(data, &encoderStream); err != nil {
			return nil, err
		}

		return json.Marshal(encoderStream.GetConfig())
	}
	return nil, errorgt.ErrUnknownTypeStream
}

// need update if new type have been added
func getToFrontFromDBModels(data json.RawMessage) (*media.StreamType, json.RawMessage, error) {
	var istream gofastocloud_models.IStream
	if err := json.Unmarshal(data, &istream); err != nil {
		return nil, nil, err
	}

	if istream.TypeStream == constans.PROXY_STR {
		var proxyStream gofastocloud_models.ProxyStream
		if err := json.Unmarshal(data, &proxyStream); err != nil {
			return nil, nil, err
		}

		front := proxyStream.ToFront()
		js, err := front.ToBytes()
		return &front.Type, js, err
	} else if istream.TypeStream == constans.VOD_PROXY_STR {
		var proxyStream gofastocloud_models.VodProxyStream
		if err := json.Unmarshal(data, &proxyStream); err != nil {
			return nil, nil, err
		}

		front := proxyStream.ToFront()
		js, err := front.ToBytes()
		return &front.Type, js, err
	} else if istream.TypeStream == constans.RELAY_STR {
		var relayStream gofastocloud_models.RelayStream
		if err := json.Unmarshal(data, &relayStream); err != nil {
			return nil, nil, err
		}

		front := relayStream.ToFront()
		js, err := front.ToBytes()
		return &front.Type, js, err
	} else if istream.TypeStream == constans.VOD_RELAY_STR {
		var relayStream gofastocloud_models.VodRelayStream
		if err := json.Unmarshal(data, &relayStream); err != nil {
			return nil, nil, err
		}

		front := relayStream.ToFront()
		js, err := front.ToBytes()
		return &front.Type, js, err
	} else if istream.TypeStream == constans.ENCODE_STR {
		var encoderStream gofastocloud_models.EncodeStream
		if err := json.Unmarshal(data, &encoderStream); err != nil {
			return nil, nil, err
		}

		front := encoderStream.ToFront()
		js, err := front.ToBytes()
		return &front.Type, js, err
	} else if istream.TypeStream == constans.VOD_ENCODE_STR {
		var encoderStream gofastocloud_models.VodEncodeStream
		if err := json.Unmarshal(data, &encoderStream); err != nil {
			return nil, nil, err
		}

		front := encoderStream.ToFront()
		js, err := front.ToBytes()
		return &front.Type, js, err
	}
	return nil, nil, errorgt.ErrUnknownTypeStream
}

// Streams routes
func (app *App) StartStream(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.startStreamImpl(sBody, w, r)
}

func (app *App) startStreamImpl(config []byte, w http.ResponseWriter, r *http.Request) {
	if !app.client.IsActive() {
		respondWithError(w, http.StatusServiceUnavailable, errorgt.MakeErrorJsonServiceNotActived(nil))
		return
	}

	base, err := media.ParseBaseConfig(config)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	if base.Type == media.STREAM_TYPE_PROXY {
		app.handleStartProxyStream(config, w)
		return
	} else if base.Type == media.STREAM_TYPE_VOD_PROXY {
		app.handleStartVodProxyStream(config, w)
		return
	} else if base.Type == media.STREAM_TYPE_RELAY {
		app.handleStartRestreamStream(config, w)
		return
	} else if base.Type == media.STREAM_TYPE_ENCODE {
		app.handleStartEncodeStream(config, w)
		return
	} else if base.Type == media.STREAM_TYPE_TIMESHIFT_PLAYER {
		app.handleStartTimeshiftPlayerStream(config, w)
		return
	} else if base.Type == media.STREAM_TYPE_TIMESHIFT_RECORDER {
		app.handleStartTimeshiftRecorderStream(config, w)
		return
	} else if base.Type == media.STREAM_TYPE_CATCHUP {
		app.handleStartCatchupStream(config, w)
		return
	} else if base.Type == media.STREAM_TYPE_TEST_LIFE {
		app.handleStartTestLifeStream(config, w)
		return
	} else if base.Type == media.STREAM_TYPE_VOD_RELAY {
		app.handleStartVodRelayStream(config, w)
		return
	} else if base.Type == media.STREAM_TYPE_VOD_ENCODE {
		app.handleStartVodEncodeStream(config, w)
		return
	} else if base.Type == media.STREAM_TYPE_COD_RELAY {
		app.handleStartCodRelayStream(config, w)
		return
	} else if base.Type == media.STREAM_TYPE_COD_ENCODE {
		app.handleStartCodEncodeStream(config, w)
		return
	} else if base.Type == media.STREAM_TYPE_EVENT {
		app.handleStartEventStream(config, w)
		return
	} else if base.Type == media.STREAM_TYPE_CV_DATA {
		app.handleStartCvDataStream(config, w)
		return
	} else if base.Type == media.STREAM_TYPE_CHANGER_RELAY {
		app.handleStartChangerRelayStream(config, w)
		return
	} else if base.Type == media.STREAM_TYPE_CHANGER_ENCODE {
		app.handleStartChangerEncodeStream(config, w)
		return
	}

	msg := fmt.Sprintf("not handed stream type: %d", base.Type)
	respondWithError(w, http.StatusNotFound, errorgt.MakeErrorJsonNotFound(&msg))
}

func (app *App) handleStartTimeshiftPlayerStream(data []byte, w http.ResponseWriter) {
	config, err := media.ParseTimeshiftPlayerConfig(data)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleStartStream(config.BaseConfig, media.NewStartTimeshiftPlayerRequest(data), w)
}

func (app *App) handleStartTimeshiftRecorderStream(data []byte, w http.ResponseWriter) {
	config, err := media.ParseTimeshiftRecorderConfig(data)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleStartStream(config.BaseConfig, media.NewStartTimeshiftRecorderRequest(data), w)
}

func (app *App) handleStartCatchupStream(data []byte, w http.ResponseWriter) {
	config, err := media.ParseCatchupConfig(data)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleStartStream(config.BaseConfig, media.NewStartCatchupRequest(data), w)
}

func (app *App) handleStartTestLifeStream(data []byte, w http.ResponseWriter) {
	config, err := media.ParseTestLifeConfig(data)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleStartStream(config.BaseConfig, media.NewStartTestLifeRequest(data), w)
}

func (app *App) handleStartVodRelayStream(data []byte, w http.ResponseWriter) {
	config, err := media.ParseVodRelayConfig(data)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleStartStream(config.BaseConfig, media.NewStartVodRelayRequest(data), w)
}

func (app *App) handleStartVodEncodeStream(data []byte, w http.ResponseWriter) {
	config, err := media.ParseVodEncodeConfig(data)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleStartStream(config.BaseConfig, media.NewStartVodEncodeRequest(data), w)
}

func (app *App) handleStartCodRelayStream(data []byte, w http.ResponseWriter) {
	config, err := media.ParseCodRelayConfig(data)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleStartStream(config.BaseConfig, media.NewStartCodRelayRequest(data), w)
}

func (app *App) handleStartCodEncodeStream(data []byte, w http.ResponseWriter) {
	config, err := media.ParseCodEncodeConfig(data)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleStartStream(config.BaseConfig, media.NewStartCodEncodeRequest(data), w)
}

func (app *App) handleStartEventStream(data []byte, w http.ResponseWriter) {
	config, err := media.ParseEventConfig(data)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleStartStream(config.BaseConfig, media.NewStartEventRequest(data), w)
}

func (app *App) handleStartCvDataStream(data []byte, w http.ResponseWriter) {
	config, err := media.ParseCvDataConfig(data)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleStartStream(config.BaseConfig, media.NewStartCvDataRequest(data), w)
}

func (app *App) handleStartChangerRelayStream(data []byte, w http.ResponseWriter) {
	config, err := media.ParseChangerRelayConfig(data)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleStartStream(config.BaseConfig, media.NewStartChangerRelayequest(data), w)
}

func (app *App) handleStartChangerEncodeStream(data []byte, w http.ResponseWriter) {
	config, err := media.ParseChangerRelayConfig(data)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleStartStream(config.BaseConfig, media.NewStartChangerEncodeRequest(data), w)
}

func (app *App) handleStartEncodeStream(data []byte, w http.ResponseWriter) {
	config, err := media.ParseEncodeConfig(data)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleStartStream(config.BaseConfig, media.NewStartEncodeStreamRequest(data), w)
}

func (app *App) handleStartRestreamStream(data []byte, w http.ResponseWriter) {
	config, err := media.ParseRestreamConfig(data)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleStartStream(config.BaseConfig, media.NewStartRestreamStreamRequest(data), w)
}

func (app *App) handleStartVodProxyStream(data []byte, w http.ResponseWriter) {
	config, err := media.ParseVodProxyConfig(data)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleStartStream(config.BaseConfig, media.NewStartVodProxyStreamRequest(data), w)
}

func (app *App) handleStartProxyStream(data []byte, w http.ResponseWriter) {
	config, err := media.ParseProxyConfig(data)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleStartStream(config.BaseConfig, media.NewStartProxyStreamRequest(data), w)
}

func (app *App) handleStartStream(config media.BaseConfig, request *media.StartStreamRequest, w http.ResponseWriter) {
	cfg := common.NewStreamConfig(request.Config, config)
	respWait := make(chan gofastocloud_base.Response)
	_, err := app.client.StartStreamWithCallback(request, func(req *gofastocloud_base.Request, resp *gofastocloud_base.Response) {
		respWait <- *resp
	})

	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	resp := <-respWait
	if resp.IsMessage() {
		respondWithOk(w)
		app.OnStreamStarted(cfg) // ok
		return
	}
	msg := resp.Error.Error()
	respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
}

func (app *App) DBStreamAdd(w http.ResponseWriter, r *http.Request) {
	if err := app.auth.ChechIsAuthHttpRequest(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleAddOrEditStreamFromRequest(sBody, w, r)
}

// need update if new type have been added
func (app *App) handleAddOrEditStreamFromRequest(body []byte, w http.ResponseWriter, r *http.Request) {
	var istream front.IStreamFront
	if err := json.Unmarshal(body, &istream); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	isAdd := istream.Id != nil
	if istream.Type == media.STREAM_TYPE_PROXY {
		if isAdd {
			app.handleEditProxyStream(body, w)
		} else {
			app.handleAddProxyStream(body, w)
		}
	} else if istream.Type == media.STREAM_TYPE_VOD_PROXY {
		if isAdd {
			app.handleEditVodProxyStream(body, w)
		} else {
			app.handleAddVodProxyStream(body, w)
		}
	} else if istream.Type == media.STREAM_TYPE_RELAY {
		if isAdd {
			app.handleEditRelayStream(body, w)
		} else {
			app.handleAddRelayStream(body, w)
		}
	} else if istream.Type == media.STREAM_TYPE_VOD_RELAY {
		if isAdd {
			app.handleEditVodRelayStream(body, w)
		} else {
			app.handleAddVodRelayStream(body, w)
		}
	} else if istream.Type == media.STREAM_TYPE_ENCODE {
		if isAdd {
			app.handleEditEncodeStream(body, w)
		} else {
			app.handleAddEncodeStream(body, w)
		}
	} else if istream.Type == media.STREAM_TYPE_VOD_ENCODE {
		if isAdd {
			app.handleEditVodEncodeStream(body, w)
		} else {
			app.handleAddVodEncodeStream(body, w)
		}
	} else {
		msg := fmt.Sprintf("not found stream type: %d", istream.Type)
		respondWithError(w, http.StatusNotFound, errorgt.MakeErrorJsonNotFound(&msg))
	}
}

func (app *App) handleAddEncodeStream(body []byte, w http.ResponseWriter) {
	var streamRequest front.EncodeStreamFront
	if err := json.Unmarshal(body, &streamRequest); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	stream, err := gofastocloud_models.MakeEncodeStreamFromFront(&streamRequest)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	stream.ID = primitive.NewObjectID()
	current := time.Now()
	stream.CreatedDate = &current
	stream.StableForStreaming()
	store, err := stream.ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	front, err := stream.ToFront().ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// save to store
	err = app.storage.Set(stream.ID.Hex(), store)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	app.wsUpdatesManager.BroadcastSendStreamDBAdded(front)
	respondWithOk(w)
}

func (app *App) handleAddVodEncodeStream(body []byte, w http.ResponseWriter) {
	var streamRequest front.VodEncodeStreamFront
	if err := json.Unmarshal(body, &streamRequest); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	stream, err := gofastocloud_models.MakeVodEncodeStreamFromFront(&streamRequest)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	stream.ID = primitive.NewObjectID()
	current := time.Now()
	stream.CreatedDate = &current
	stream.StableForStreaming()
	store, err := stream.ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	front, err := stream.ToFront().ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// save to store
	err = app.storage.Set(stream.ID.Hex(), store)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	app.wsUpdatesManager.BroadcastSendStreamDBAdded(front)
	respondWithOk(w)
}

func (app *App) handleAddProxyStream(body []byte, w http.ResponseWriter) {
	var streamRequest front.ProxyStreamFront
	if err := json.Unmarshal(body, &streamRequest); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	stream, err := gofastocloud_models.MakeProxyStreamFromFront(&streamRequest)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	stream.ID = primitive.NewObjectID()
	current := time.Now()
	stream.CreatedDate = &current
	store, err := stream.ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	front, err := stream.ToFront().ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// save to store
	err = app.storage.Set(stream.ID.Hex(), store)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	app.wsUpdatesManager.BroadcastSendStreamDBAdded(front)
	respondWithOk(w)
}

func (app *App) handleAddRelayStream(body []byte, w http.ResponseWriter) {
	var streamRequest front.RelayStreamFront
	if err := json.Unmarshal(body, &streamRequest); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	stream, err := gofastocloud_models.MakeRelayStreamFromFront(&streamRequest)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	stream.ID = primitive.NewObjectID()
	current := time.Now()
	stream.CreatedDate = &current
	stream.StableForStreaming()
	store, err := stream.ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	front, err := stream.ToFront().ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// save to store
	err = app.storage.Set(stream.ID.Hex(), store)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	app.wsUpdatesManager.BroadcastSendStreamDBAdded(front)
	respondWithOk(w)
}

func (app *App) handleAddVodRelayStream(body []byte, w http.ResponseWriter) {
	var streamRequest front.VodRelayStreamFront
	if err := json.Unmarshal(body, &streamRequest); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	stream, err := gofastocloud_models.MakeVodRelayStreamFromFront(&streamRequest)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	stream.ID = primitive.NewObjectID()
	current := time.Now()
	stream.CreatedDate = &current
	stream.StableForStreaming()
	store, err := stream.ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	front, err := stream.ToFront().ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// save to store
	err = app.storage.Set(stream.ID.Hex(), store)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	app.wsUpdatesManager.BroadcastSendStreamDBAdded(front)
	respondWithOk(w)
}

func (app *App) handleAddVodProxyStream(body []byte, w http.ResponseWriter) {
	var streamRequest front.VodProxyStreamFront
	if err := json.Unmarshal(body, &streamRequest); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	stream, err := gofastocloud_models.MakeVodProxyStreamFromFront(&streamRequest)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	stream.ID = primitive.NewObjectID()
	current := time.Now()
	stream.CreatedDate = &current
	store, err := stream.ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	front, err := stream.ToFront().ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// save to store
	err = app.storage.Set(stream.ID.Hex(), store)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	app.wsUpdatesManager.BroadcastSendStreamDBAdded(front)
	respondWithOk(w)
}

func (app *App) handleEditProxyStream(body []byte, w http.ResponseWriter) {
	var streamRequest front.ProxyStreamFront
	if err := json.Unmarshal(body, &streamRequest); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	stream, err := gofastocloud_models.MakeProxyStreamFromFront(&streamRequest)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	store, err := stream.ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	front, err := stream.ToFront().ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// save to store
	err = app.storage.Set(stream.ID.Hex(), store)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	app.wsUpdatesManager.BroadcastSendStreamDBUpdated(front)
	respondWithOk(w)
}

func (app *App) handleEditRelayStream(body []byte, w http.ResponseWriter) {
	var streamRequest front.RelayStreamFront
	if err := json.Unmarshal(body, &streamRequest); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	stream, err := gofastocloud_models.MakeRelayStreamFromFront(&streamRequest)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	stream.StableForStreaming()
	store, err := stream.ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	front, err := stream.ToFront().ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// save to store
	err = app.storage.Set(stream.ID.Hex(), store)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	app.wsUpdatesManager.BroadcastSendStreamDBUpdated(front)
	respondWithOk(w)
}

func (app *App) handleEditVodRelayStream(body []byte, w http.ResponseWriter) {
	var streamRequest front.VodRelayStreamFront
	if err := json.Unmarshal(body, &streamRequest); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	stream, err := gofastocloud_models.MakeVodRelayStreamFromFront(&streamRequest)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	stream.StableForStreaming()
	store, err := stream.ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}
	front, err := stream.ToFront().ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// save to store
	err = app.storage.Set(stream.ID.Hex(), store)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	app.wsUpdatesManager.BroadcastSendStreamDBUpdated(front)
	respondWithOk(w)
}

func (app *App) handleEditVodProxyStream(body []byte, w http.ResponseWriter) {
	var streamRequest front.VodProxyStreamFront
	if err := json.Unmarshal(body, &streamRequest); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	stream, err := gofastocloud_models.MakeVodProxyStreamFromFront(&streamRequest)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	store, err := stream.ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}
	front, err := stream.ToFront().ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// save to store
	err = app.storage.Set(stream.ID.Hex(), store)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	app.wsUpdatesManager.BroadcastSendStreamDBUpdated(front)
	respondWithOk(w)
}

func (app *App) handleEditEncodeStream(body []byte, w http.ResponseWriter) {
	var streamRequest front.EncodeStreamFront
	if err := json.Unmarshal(body, &streamRequest); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	stream, err := gofastocloud_models.MakeEncodeStreamFromFront(&streamRequest)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	stream.StableForStreaming()
	store, err := stream.ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	front, err := stream.ToFront().ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// save to store
	err = app.storage.Set(stream.ID.Hex(), store)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	app.wsUpdatesManager.BroadcastSendStreamDBUpdated(front)
	respondWithOk(w)
}

func (app *App) handleEditVodEncodeStream(body []byte, w http.ResponseWriter) {
	var streamRequest front.VodEncodeStreamFront
	if err := json.Unmarshal(body, &streamRequest); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	stream, err := gofastocloud_models.MakeVodEncodeStreamFromFront(&streamRequest)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	stream.StableForStreaming()
	store, err := stream.ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	front, err := stream.ToFront().ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// save to store
	err = app.storage.Set(stream.ID.Hex(), store)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	app.wsUpdatesManager.BroadcastSendStreamDBUpdated(front)
	respondWithOk(w)
}

func (app *App) DBStreamRemove(w http.ResponseWriter, r *http.Request) {
	if err := app.auth.ChechIsAuthHttpRequest(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	vars := mux.Vars(r)
	sid := vars["id"]
	store, err := app.storage.Get(sid)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusNotFound, errorgt.MakeErrorJsonNotFound(&msg))
		return
	}

	_, front, err := getToFrontFromDBModels(store)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	if err := app.storage.Delete(sid); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	app.wsUpdatesManager.BroadcastSendStreamDBRemoved(front)
	respondWithOk(w)
}

func (app *App) DBStreamStart(w http.ResponseWriter, r *http.Request) {
	if err := app.auth.ChechIsAuthHttpRequest(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	vars := mux.Vars(r)
	sid := vars["id"]
	data, err := app.storage.Get(sid)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusNotFound, errorgt.MakeErrorJsonNotFound(&msg))
		return
	}

	config, err := getConfigFromDBModels(data)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}
	app.startStreamImpl(config, w, r)
}

func (app *App) DBStreamUpdate(w http.ResponseWriter, r *http.Request) {
	if err := app.auth.ChechIsAuthHttpRequest(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleAddOrEditStreamFromRequest(sBody, w, r)
}

func (app *App) DBStreamsList(w http.ResponseWriter, r *http.Request) {
	if err := app.auth.ChechIsAuthHttpRequest(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	dbStreams, err := app.storage.GetStreams()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	type StreamsResponce struct {
		Streams []json.RawMessage `json:"streams"`
		Vods    []json.RawMessage `json:"vods"`
	}

	streams := []json.RawMessage{}
	vods := []json.RawMessage{}
	for _, stream := range dbStreams {
		st, front, err := getToFrontFromDBModels(stream)
		if err != nil {
			continue
		}

		if media.IsVodStreamType(*st) {
			vods = append(vods, front)
		} else {
			streams = append(streams, front)
		}
	}

	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(StreamsResponce{Streams: streams, Vods: vods}))
}

func (app *App) DBStreamsAddView(w http.ResponseWriter, r *http.Request) {
	if err := app.auth.ChechIsAuthHttpRequest(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}
	params := mux.Vars(r)
	stream, err := app.storage.Get(params["id"])
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}
	if err := app.handleAddViewStream(stream); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}
	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(nil))
}

func (app *App) handleAddViewStream(data json.RawMessage) error {
	var istream gofastocloud_models.IStream
	if err := json.Unmarshal(data, &istream); err != nil {
		return err
	}
	if istream.TypeStream == constans.PROXY_STR {
		var stream gofastocloud_models.ProxyStream
		if err := json.Unmarshal(data, &stream); err != nil {
			return err
		}
		stream.ViewCount++
		store, err := stream.ToBytes()
		if err != nil {
			return err
		}

		if err := app.storage.Set(stream.ID.Hex(), store); err != nil {
			return err
		}
		return nil
	} else if istream.TypeStream == constans.VOD_PROXY_STR {
		var stream gofastocloud_models.VodProxyStream
		if err := json.Unmarshal(data, &stream); err != nil {
			return err
		}
		stream.ViewCount++
		store, err := stream.ToBytes()
		if err != nil {
			return err
		}
		if err := app.storage.Set(stream.ID.Hex(), store); err != nil {
			return err
		}
		return nil
	} else if istream.TypeStream == constans.RELAY_STR {
		var stream gofastocloud_models.RelayStream
		if err := json.Unmarshal(data, &stream); err != nil {
			return err
		}
		stream.ViewCount++
		store, err := stream.ToBytes()
		if err != nil {
			return err
		}

		if err := app.storage.Set(stream.ID.Hex(), store); err != nil {
			return err
		}
		return nil
	} else if istream.TypeStream == constans.VOD_RELAY_STR {
		var stream gofastocloud_models.VodRelayStream
		if err := json.Unmarshal(data, &stream); err != nil {
			return err
		}
		stream.ViewCount++
		store, err := stream.ToBytes()
		if err != nil {
			return err
		}
		if err := app.storage.Set(stream.ID.Hex(), store); err != nil {
			return err
		}
		return nil
	} else if istream.TypeStream == constans.ENCODE_STR {
		var stream gofastocloud_models.EncodeStream
		if err := json.Unmarshal(data, &stream); err != nil {
			return err
		}
		stream.ViewCount++
		store, err := stream.ToBytes()
		if err != nil {
			return err
		}
		if err := app.storage.Set(stream.ID.Hex(), store); err != nil {
			return err
		}
		return nil
	} else if istream.TypeStream == constans.VOD_ENCODE_STR {
		var stream gofastocloud_models.VodEncodeStream
		if err := json.Unmarshal(data, &stream); err != nil {
			return err
		}
		stream.ViewCount++
		store, err := stream.ToBytes()
		if err != nil {
			return err
		}
		if err := app.storage.Set(stream.ID.Hex(), store); err != nil {
			return err
		}
		return nil
	}
	return errorgt.ErrUnknownTypeStream
}

func (app *App) RestartStream(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	var body public.RestartStreamRequest
	err = json.Unmarshal(sBody, &body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	restart := media.NewRestartStreamRequest(body.Id)
	respWait := make(chan gofastocloud_base.Response)
	_, err = app.client.RestartStreamWithCallback(restart, func(req *gofastocloud_base.Request, resp *gofastocloud_base.Response) {
		respWait <- *resp
	})

	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	resp := <-respWait
	if resp.IsMessage() {
		respondWithOk(w)
		return
	}
	msg := resp.Error.Error()
	respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
}

func (app *App) StopStream(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	var req public.StopStreamRequest
	err = json.Unmarshal(sBody, &req)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	respWait := make(chan gofastocloud_base.Response)
	stop := media.NewStopStreamRequest(req.Id, req.Force)
	_, err = app.client.StopStreamWithCallback(stop, func(req *gofastocloud_base.Request, resp *gofastocloud_base.Response) {
		respWait <- *resp
	})

	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	resp := <-respWait
	if resp.IsMessage() {
		respondWithOk(w)
		return
	}
	msg := resp.Error.Error()
	respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
}

func (app *App) CleanStream(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	if !app.client.IsActive() {
		respondWithError(w, http.StatusServiceUnavailable, errorgt.MakeErrorJsonServiceNotActived(nil))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	_, err = media.ParseBaseConfig(sBody)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	config := json.RawMessage(sBody)
	clean := media.NewCleanStreamRequest(config)
	respWait := make(chan gofastocloud_base.Response)
	_, err = app.client.CleanStreamWithCallback(clean, func(req *gofastocloud_base.Request, resp *gofastocloud_base.Response) {
		respWait <- *resp
	})

	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	resp := <-respWait
	if resp.IsMessage() {
		respondWithOk(w)
		return
	}
	msg := resp.Error.Error()
	respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
}

func (app *App) ChangeSourceStream(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	if !app.client.IsActive() {
		respondWithError(w, http.StatusServiceUnavailable, errorgt.MakeErrorJsonServiceNotActived(nil))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	var body public.ChangeInputStreamRequest
	err = json.Unmarshal(sBody, &body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}
	change := media.NewChangeInputStreamRequest(body.Sid, body.ChannelId)

	respWait := make(chan gofastocloud_base.Response)
	_, err = app.client.ChangeInputStreamWithCallback(change, func(req *gofastocloud_base.Request, resp *gofastocloud_base.Response) {
		respWait <- *resp
	})

	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	resp := <-respWait
	if resp.IsMessage() {
		respondWithOk(w)
		return
	}
	msg := resp.Error.Error()
	respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
}

func (app *App) StreamStats(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	if !app.client.IsActive() {
		respondWithError(w, http.StatusServiceUnavailable, errorgt.MakeErrorJsonServiceNotActived(nil))
		return
	}

	vars := mux.Vars(r)
	sid := vars["id"]
	app.active_streams_mutex.Lock()
	found, ok := app.active_streams[media.StreamId(sid)]
	app.active_streams_mutex.Unlock()
	if !ok {
		msg := fmt.Sprintf("stream with id: %s", sid)
		respondWithError(w, http.StatusNotFound, errorgt.MakeErrorJsonNotFound(&msg))
		return
	}

	resp := public.StreamStatistic{Stats: found.Statistic}
	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(resp))
}
func (app *App) StreamEmbed(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	params := r.URL.Query().Get("src")
	self := app.makeSelfUrl(kServerPrefix+"/stream/player", r.Host)
	url := fmt.Sprintf(`<iframe src="%s?src=%v" frameborder="0" style="position:absolute;top:0px;left:0px;right:0px;height="100%%" width="100%%"></iframe>`, self, params)
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(url))
}

func (app *App) StreamPlayer(w http.ResponseWriter, r *http.Request) {
	link := r.URL.Query().Get("src")
	tmpl, err := template.New("player.html").ParseFiles("install/template/player.html")
	if err != nil {
		msg := "error tempalte load"
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	type response struct {
		Link string
	}

	respondWithTemplate(w, tmpl, response{Link: link})
}

func (app *App) StreamConfig(w http.ResponseWriter, r *http.Request) {
	/*err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}*/

	vars := mux.Vars(r)
	sid := vars["id"]
	selfUrl := app.makeSelfUrl(kStreamConfigUploadRoute+"/"+sid, r.Host)
	mid := media.StreamId(sid)
	conf := media.NewGetConfigJsonStreamRequest(mid, media.MakeFeedbackDir(mid), selfUrl)
	respWait := make(chan gofastocloud_base.Response)
	_, err := app.client.GetConfigJsonStreamWithCallback(conf, func(req *gofastocloud_base.Request, resp *gofastocloud_base.Response) {
		respWait <- *resp
	})

	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	resp := <-respWait
	if resp.IsMessage() {
		logPath, err := app.getStreamConfigFilePath(sid)
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		dat, err := ioutil.ReadFile(*logPath)
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		resp := public.StreamConfig{Config: dat}
		respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(resp))
		return
	}
	msg := resp.Error.Error()
	respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
}

func (app *App) ExternalStreamUploadConfig(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	sid := vars["id"]

	file_path, err := app.getStreamConfigFilePath(sid)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// Create a new file in the uploads directory
	dst, err := os.Create(*file_path)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// Copy the uploaded file to the filesystem
	// at the specified destination
	_, err = io.Copy(dst, r.Body)
	dst.Close()
	if err != nil {
		os.Remove(*file_path)
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(nil))
}

func (app *App) StreamLogs(w http.ResponseWriter, r *http.Request) {
	/*err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}*/

	vars := mux.Vars(r)
	sid := vars["id"]
	selfUrl := app.makeSelfUrl(kStreamLogsUploadRoute+"/"+sid, r.Host)
	mid := media.StreamId(sid)
	logs := media.NewGetLogStreamRequest(mid, media.MakeFeedbackDir(mid), selfUrl)
	respWait := make(chan gofastocloud_base.Response)
	_, err := app.client.GetLogStreamWithCallback(logs, func(req *gofastocloud_base.Request, resp *gofastocloud_base.Response) {
		respWait <- *resp
	})

	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	resp := <-respWait
	if resp.IsMessage() {
		logPath, err := app.getStreamLogsFilePath(sid)
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		http.ServeFile(w, r, *logPath)
		return
	}
	msg := resp.Error.Error()
	respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
}

func (app *App) ExternalStreamUploadLogs(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	sid := vars["id"]

	file_path, err := app.getStreamLogsFilePath(sid)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// Create a new file in the uploads directory
	dst, err := os.Create(*file_path)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// Copy the uploaded file to the filesystem
	// at the specified destination
	dst.Write([]byte(`<pre>`))
	_, err = io.Copy(dst, r.Body)
	dst.Write([]byte(`</pre>`))
	dst.Close()
	if err != nil {
		os.Remove(*file_path)
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(nil))
}

func (app *App) StreamPipeline(w http.ResponseWriter, r *http.Request) {
	/*err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}*/

	vars := mux.Vars(r)
	sid := vars["id"]
	selfUrl := app.makeSelfUrl(kStreamPipelineUploadRoute+"/"+sid, r.Host)
	mid := media.StreamId(sid)
	pipe := media.NewGetPipelineStreamRequest(mid, media.MakeFeedbackDir(mid), selfUrl)
	respWait := make(chan gofastocloud_base.Response)
	_, err := app.client.GetPipelineStreamWithCallback(pipe, func(req *gofastocloud_base.Request, resp *gofastocloud_base.Response) {
		respWait <- *resp
	})

	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	resp := <-respWait
	if resp.IsMessage() {
		pipePath, err := app.getStreamPipelineFilePath(sid)
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		http.ServeFile(w, r, *pipePath)
		return
	}
	msg := resp.Error.Error()
	respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
}

func (app *App) ExternalStreamUploadPipeline(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	sid := vars["id"]

	file_path, err := app.getStreamPipelineFilePath(sid)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// Create a new file in the uploads directory
	dst, err := os.Create(*file_path)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// Copy the uploaded file to the filesystem
	// at the specified destination
	_, err = io.Copy(dst, r.Body)
	dst.Close()
	if err != nil {
		os.Remove(*file_path)
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(nil))
}

func (app *App) StreamsStats(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	if !app.client.IsActive() {
		respondWithError(w, http.StatusServiceUnavailable, errorgt.MakeErrorJsonServiceNotActived(nil))
		return
	}

	stats := []*media.StreamStatisticInfo{}
	app.active_streams_mutex.Lock()
	for _, v := range app.active_streams {
		if v.Statistic != nil {
			stats = append(stats, v.Statistic)
		}
	}
	app.active_streams_mutex.Unlock()

	resp := public.StreamsStatistic{Stats: stats}
	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(resp))
}

func (app *App) StreamsConfigs(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	if !app.client.IsActive() {
		respondWithError(w, http.StatusServiceUnavailable, errorgt.MakeErrorJsonServiceNotActived(nil))
		return
	}

	configs := []json.RawMessage{}
	app.active_streams_mutex.Lock()
	for _, v := range app.active_streams {
		if v.Config != nil {
			configs = append(configs, v.Config.GetRawConfig())
		}
	}
	app.active_streams_mutex.Unlock()

	resp := public.StreamConfigs{Configs: configs}
	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(resp))
}

func (app *App) InjectMasterSourceStream(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	var body public.InjectMasterInputRequest
	err = json.Unmarshal(sBody, &body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}
	inject := media.NewInjectMasterInputUrlRequest(body.Sid, body.Url)

	respWait := make(chan gofastocloud_base.Response)
	_, err = app.client.InjectMasterInputUrlWithCallback(inject, func(req *gofastocloud_base.Request, resp *gofastocloud_base.Response) {
		respWait <- *resp
	})

	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	resp := <-respWait
	if resp.IsMessage() {
		respondWithOk(w)
		return
	}
	msg := resp.Error.Error()
	respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
}

func (app *App) RemoveMasterSourceStream(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	var body public.RemoveMasterInputRequest
	err = json.Unmarshal(sBody, &body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}
	remove := media.NewRemoveMasterInputUrlRequest(body.Sid, body.Url)

	respWait := make(chan gofastocloud_base.Response)
	_, err = app.client.RemoveMasterInputUrlWithCallback(remove, func(req *gofastocloud_base.Request, resp *gofastocloud_base.Response) {
		respWait <- *resp
	})

	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	resp := <-respWait
	if resp.IsMessage() {
		respondWithOk(w)
		return
	}
	msg := resp.Error.Error()
	respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
}

func (app *App) ServeHlsRequest(w http.ResponseWriter, r *http.Request) {
	hls := app.client.GetHttpHost()
	if hls == nil {
		msg := "hls server not ready"
		respondWithError(w, http.StatusServiceUnavailable, errorgt.MakeErrorJsonServiceNotActived(&msg))
		return
	}

	if err := app.checkContentAuth(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusForbidden, errorgt.MakeErrorJsonForrbiddenAction(&msg))
		return
	}

	// external
	alias := app.config.Settings.Node.Host.Alias
	if alias != nil {
		hls.Host = *alias
	}

	newUrl := *r.URL // copy
	if r.TLS == nil {
		newUrl.Scheme = "http"
	} else {
		newUrl.Scheme = "https"
	}
	newUrl.Host = hls.String()
	http.Redirect(w, r, newUrl.String(), http.StatusFound)
}

func (app *App) ServeVodsRequest(w http.ResponseWriter, r *http.Request) {
	vods := app.client.GetVodsHost()
	if vods == nil {
		msg := "vods server not ready"
		respondWithError(w, http.StatusServiceUnavailable, errorgt.MakeErrorJsonServiceNotActived(&msg))
		return
	}

	if err := app.checkContentAuth(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusForbidden, errorgt.MakeErrorJsonForrbiddenAction(&msg))
		return
	}

	// external
	alias := app.config.Settings.Node.Host.Alias
	if alias != nil {
		vods.Host = *alias
	}

	newUrl := *r.URL // copy
	if r.TLS == nil {
		newUrl.Scheme = "http"
	} else {
		newUrl.Scheme = "https"
	}
	newUrl.Host = vods.String()
	http.Redirect(w, r, newUrl.String(), http.StatusFound)
}

func (app *App) ServeCodsRequest(w http.ResponseWriter, r *http.Request) {
	cods := app.client.GetCodsHost()
	if cods == nil {
		msg := "cods server not ready"
		respondWithError(w, http.StatusServiceUnavailable, errorgt.MakeErrorJsonServiceNotActived(&msg))
		return
	}

	if err := app.checkContentAuth(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusForbidden, errorgt.MakeErrorJsonForrbiddenAction(&msg))
		return
	}

	// external
	alias := app.config.Settings.Node.Host.Alias
	if alias != nil {
		cods.Host = *alias
	}

	urlPart := strings.Split(r.URL.Path, "/")
	if len(urlPart) > 2 {
		sid := media.StreamId(urlPart[2])
		if stream := app.FindActiveStream(media.StreamId(sid)); stream != nil { // trying to find config
			if !stream.IsActive() {
				app.processCodStart(stream.Config)
			}
		}
	}

	newUrl := *r.URL // copy
	if r.TLS == nil {
		newUrl.Scheme = "http"
	} else {
		newUrl.Scheme = "https"
	}
	newUrl.Host = cods.String()
	http.Redirect(w, r, newUrl.String(), http.StatusFound)
}

func (app *App) StartCached(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	if !app.client.IsActive() {
		respondWithError(w, http.StatusServiceUnavailable, errorgt.MakeErrorJsonServiceNotActived(nil))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	var body public.StartCachedStreamRequest
	err = json.Unmarshal(sBody, &body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	sid := body.Id
	stream := app.FindActiveStream(sid)
	if stream == nil { // trying to find config
		msg := fmt.Sprintf("stream with id: %s", sid)
		respondWithError(w, http.StatusNotFound, errorgt.MakeErrorJsonNotFound(&msg))
		return
	}

	app.startStreamImpl(stream.Config.GetRawConfig(), w, r)
}

func (app *App) GetContentToken(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}
	host, _, err := gofastogt.GetIPFromRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}
	token, err := app.contentAuth.GetAuthContentToken(*host)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}
	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(*token))
}

func (app *App) processCodStart(stream *common.StreamConfig) { // not need OnStreamStarted because already in active_streams array
	jsonData := stream.GetRawConfig()
	base := stream.GetConfig()
	var request *media.StartStreamRequest
	var timeoutHls int = 0
	if base.Type == media.STREAM_TYPE_COD_RELAY {
		config, err := media.ParseCodRelayConfig(jsonData)
		if err == nil {
			for _, out := range config.Output {
				if out.IsHls() {
					if out.ChunkDuration != nil {
						timeoutHls = gofastogt.MaxInt(timeoutHls, *out.ChunkDuration)
					} else {
						timeoutHls = gofastogt.MaxInt(timeoutHls, kDefaultCodHlsChunkDuration)
					}
				}
			}
		}
		lrequest := media.NewStartCodRelayRequest(jsonData)
		request = lrequest
	} else if base.Type == media.STREAM_TYPE_COD_ENCODE {
		config, err := media.ParseCodEncodeConfig(jsonData)
		if err == nil {
			for _, out := range config.Output {
				if out.IsHls() {
					if out.ChunkDuration != nil {
						timeoutHls = gofastogt.MaxInt(timeoutHls, *out.ChunkDuration)
					} else {
						timeoutHls = gofastogt.MaxInt(timeoutHls, kDefaultCodHlsChunkDuration)
					}
				}
			}
		}
		lrequest := media.NewStartCodEncodeRequest(jsonData)
		request = lrequest
	}

	if request != nil {
		respWait := make(chan gofastocloud_base.Response)
		_, err := app.client.StartStreamWithCallback(request, func(req *gofastocloud_base.Request, resp *gofastocloud_base.Response) {
			respWait <- *resp
		})

		if err != nil {
		} else {
			resp := <-respWait
			if resp.IsMessage() {
				if timeoutHls != 0 {
					duration := time.Duration(timeoutHls+1) * time.Second // +1 because time to start
					time.Sleep(duration)
				}
			} else {
			}
		}
	}
}

func (app *App) checkContentAuth(r *http.Request) error {
	_, sid, oid, fileName, err := utils.SplitHlsUrl(r.URL.Path[1:])
	if err != nil {
		return err
	}

	if !utils.IsHlsFileName(fileName) {
		return fmt.Errorf("invalid input")
	}

	jsonStream, err := app.storage.Get(sid)
	if err != nil {
		return err
	}

	var istream gofastocloud_models.IStream
	if err := json.Unmarshal(jsonStream, &istream); err != nil {
		return err
	}

	if *istream.Output[oid].Token {
		host, _, err := gofastogt.GetIPFromRequest(r)
		if err != nil {
			return err
		}
		token := r.URL.Query().Get("token")
		if err := app.contentAuth.CheckAuthContent(token, *host); err != nil {
			return err
		}
	}
	return nil
}

func (app *App) getHardawareStreams() []json.RawMessage {
	dbStreams, err := app.storage.GetStreams()
	if err != nil {
		log.Errorf("error autostart streams. Failed to get streams from store. Error: %v", err.Error())
		return nil
	}
	var streams []json.RawMessage
	for _, stream := range dbStreams {
		var istream gofastocloud_models.IStream
		if err := json.Unmarshal(stream, &istream); err != nil {
			log.Errorf("error autostart streams. Failed parce dbStream to model. error: %v", err.Error())
			continue
		}
		if istream.StreamType() == media.STREAM_TYPE_PROXY {
			continue
		} else if istream.StreamType() == media.STREAM_TYPE_VOD_PROXY {
			continue
		} else if istream.StreamType() == media.STREAM_TYPE_RELAY {
			var relayStream gofastocloud_models.RelayStream
			if err := json.Unmarshal(stream, &relayStream); err != nil {
				log.Errorf("error autostart streams. Failed parce dbStream to RelayStream. error: %v", err.Error())
				continue
			}
			if relayStream.AutoStart {
				jsonRelayConfig, err := json.Marshal(relayStream.GetConfig())
				if err != nil {
					log.Errorf("error autostart streams. Failed get config. error: %v", err.Error())
					continue
				}
				streams = append(streams, jsonRelayConfig)
			}
		} else if istream.StreamType() == media.STREAM_TYPE_ENCODE {
			var encodeStream gofastocloud_models.EncodeStream
			if err := json.Unmarshal(stream, &encodeStream); err != nil {
				log.Errorf("error autostart streams. Failed parce dbStream to EncodeStream. error: %v", err.Error())
				continue
			}
			if encodeStream.AutoStart {
				jsonEncoderConfig, err := json.Marshal(encodeStream.GetConfig())
				if err != nil {
					log.Errorf("error autostart streams. Failed get config. error: %v", err.Error())
					continue
				}
				streams = append(streams, jsonEncoderConfig)
			}
		} else if istream.StreamType() == media.STREAM_TYPE_VOD_RELAY {
			var vodRelayStream gofastocloud_models.VodRelayStream
			if err := json.Unmarshal(stream, &vodRelayStream); err != nil {
				log.Errorf("error autostart streams. Failed parce dbStream to VodRelayStream. error: %v", err.Error())
				continue
			}
			if vodRelayStream.AutoStart {
				jsonVodRelayConfig, err := json.Marshal(vodRelayStream.GetConfig())
				if err != nil {
					log.Errorf("error autostart streams. Failed get config. error: %v", err.Error())
					continue
				}
				streams = append(streams, jsonVodRelayConfig)
			}
		} else if istream.StreamType() == media.STREAM_TYPE_VOD_ENCODE {
			var vodEncodeStream gofastocloud_models.VodEncodeStream
			if err := json.Unmarshal(stream, &vodEncodeStream); err != nil {
				log.Errorf("error autostart streams. Failed parce dbStream to VodEncodeStream. error: %v", err.Error())
				continue
			}
			if vodEncodeStream.AutoStart {
				jsonVodEncoderConfig, err := json.Marshal(vodEncodeStream.GetConfig())
				if err != nil {
					log.Errorf("error autostart streams. Failed get config. error: %v", err.Error())
					continue
				}
				streams = append(streams, jsonVodEncoderConfig)
			}
		} else {
			log.Errorf("error autostart streams. Uncnown stream type.")
		}
	}
	return streams
}
