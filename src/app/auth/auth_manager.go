package auth

import (
	"encoding/base64"
	"fmt"
	"net/http"
	"strings"
)

type IAuthManager interface {
	ChechIsAuthHttpRequest(request *http.Request) error
	ChechIsAuthHttpRequestWithToken(request *http.Request) error
}

type NoAuthManager struct {
	IAuthManager
}

func NewNoAuthManager() *NoAuthManager {
	auth := NoAuthManager{}
	return &auth
}

func (no *NoAuthManager) ChechIsAuthHttpRequest(request *http.Request) error {
	return nil
}

func (no *NoAuthManager) ChechIsAuthHttpRequestWithToken(request *http.Request) error {
	return nil
}

type Users map[string]string

type BasicAuthManager struct {
	IAuthManager

	users Users
}

func NewBasicAuthManager(users Users) *BasicAuthManager {
	return &BasicAuthManager{users: users}
}

func (basic *BasicAuthManager) ChechIsAuthHttpRequest(request *http.Request) error {
	user, pass, ok := request.BasicAuth()
	if !ok {
		return fmt.Errorf("parsing basic auth")
	}

	found, ok := basic.users[user]
	if !ok {
		return fmt.Errorf("not found user")
	}

	if found != pass {
		return fmt.Errorf("wrong password")
	}

	return nil
}

func (basic *BasicAuthManager) ChechIsAuthHttpRequestWithToken(request *http.Request) error {
	user, pass, ok := request.BasicAuth()
	if !ok {
		query := request.URL.Query()
		token := query.Get("token")
		if token == "" {
			return fmt.Errorf("parsing basic auth")
		}

		c, err := base64.StdEncoding.DecodeString(token)
		if err != nil {
			return fmt.Errorf("parsing basic auth base64")
		}
		cs := string(c)
		s := strings.IndexByte(cs, ':')
		if s < 0 {
			return fmt.Errorf("parsing basic auth creds")
		}
		user, pass = cs[:s], cs[s+1:]
	}

	found, ok := basic.users[user]
	if !ok {
		return fmt.Errorf("not found user")
	}

	if found != pass {
		return fmt.Errorf("wrong password")
	}

	return nil
}

type Master map[string]string
type MasterAuth struct {
	master Master
}

func NewMasterAuth(master Master) *MasterAuth {
	return &MasterAuth{
		master: master,
	}
}

func (m *MasterAuth) AuthMasterHttpRequest(request *http.Request) (*string, error) {
	master, pass, ok := request.BasicAuth()
	if !ok {
		return nil, fmt.Errorf("parsing basic auth")
	}

	found, ok := m.master[master]
	if !ok {
		return nil, fmt.Errorf("wrong master user")
	}

	if found != pass {
		return nil, fmt.Errorf("wrong password")
	}
	masterConf := fmt.Sprintf("%s:%s", master, pass)
	return &masterConf, nil
}
