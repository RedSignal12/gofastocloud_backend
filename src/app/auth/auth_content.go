package auth

import (
	"encoding/base64"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go/v4"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models"
	"gitlab.com/fastogt/gofastogt"
)

const kLIFE_TIME int = 180 // token life time in minuts

type ContentAuthManager interface {
	GetAuthContentToken(data string) (*string, error)
	CheckAuthContent(token, ip string) error
}

type JWTContentAuthManager struct {
	ContentAuthManager
	secret string
}

func NewJWTContentAuthManager() *JWTContentAuthManager {
	return &JWTContentAuthManager{
		secret: "fastocloud",
	}
}

func (c *JWTContentAuthManager) CheckAuthContent(token, ip string) error {
	t, err := jwt.ParseWithClaims(token, jwt.MapClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(c.secret), nil
	})
	if err != nil {
		return err
	}
	if !t.Valid {
		return fmt.Errorf("token expired")
	}
	claims, ok := t.Claims.(jwt.MapClaims)
	if !ok {
		return fmt.Errorf("token isn't valid")
	}
	claim := claims["ip"].(string)
	if claim != ip {
		return fmt.Errorf("token isn't valid")
	}
	return nil
}

func (c *JWTContentAuthManager) GetAuthContentToken(data string) (*string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["ip"] = data
	claims["exp"] = time.Now().Add(time.Minute * time.Duration(kLIFE_TIME)).Unix()
	tokenString, err := token.SignedString([]byte(c.secret))
	if err != nil {
		return nil, err
	}
	return &tokenString, nil
}

type BaseContentAuthMenager struct {
	ContentAuthManager
}

func NewBaseContentAuthManager() *BaseContentAuthMenager {
	return &BaseContentAuthMenager{}
}

func (b *BaseContentAuthMenager) CheckAuthContent(token, ip string) error {
	t, err := base64.StdEncoding.DecodeString(token)
	if err != nil {
		return fmt.Errorf("error parsing token base64")
	}
	claims := strings.Split(string(t), ":")
	if len(claims) != 2 {
		return fmt.Errorf("token isn't valid")
	}
	climeExp, err := strconv.ParseInt(claims[1], 10, 64)
	if err != nil {
		return fmt.Errorf("")
	}
	exp := gofastogt.UtcTime2Time(gofastogt.UtcTimeMsec(climeExp))
	if time.Now().After(exp) {
		return fmt.Errorf("token expired")
	}
	if ip != claims[0] {
		return fmt.Errorf("token isn't valid")
	}
	return nil
}

func (b *BaseContentAuthMenager) GetAuthContentToken(s string) (*string, error) {
	exp := gofastogt.Time2UtcTimeMsec(time.Now().Add(time.Minute * time.Duration(kLIFE_TIME)))
	if gofastogt.IsIP(s) {
		data := fmt.Sprintf("%v:%v", s, exp)
		token := base64.StdEncoding.EncodeToString([]byte(data))
		return &token, nil
	}
	return nil, fmt.Errorf("error get token")
}

func MakeAuthContent(content gofastocloud_models.AuthContent) ContentAuthManager {
	if content == gofastocloud_models.JWT_CONTENT_AUTH {
		return NewJWTContentAuthManager()
	}
	return NewBaseContentAuthManager()
}
