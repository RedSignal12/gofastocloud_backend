#!/bin/bash
set -ex

cd src

GOOS=linux GOARCH=amd64 go build -o gofastocloud_x86_64 gofastocloud.go
chmod +x gofastocloud_x86_64
GOOS=linux GOARCH=arm64 go build -o gofastocloud_arm64 gofastocloud.go
chmod +x gofastocloud_arm64
GOOS=linux GOARCH=arm go build -o gofastocloud_arm gofastocloud.go
chmod +x gofastocloud_arm
